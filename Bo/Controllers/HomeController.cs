﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Bo.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.Title = "Home Page";

			return View();
		}
		public ActionResult Guess(string q, string e, int? n)
		{
			Stopwatch w = new Stopwatch();
			//ViewBag.Title = "Dict";
			var dict = Dict.Instance();
			w.Start();
			var result = dict.Guess(q, e, n);
			w.Stop();
			return Json(new{r = result, t=w.ElapsedMilliseconds}, JsonRequestBehavior.AllowGet);
		}
	}

	public class Dict
	{
		static Dict _instance;
		public static Dict Instance()
		{
			if (_instance != null)
				return _instance;

			_instance = new Dict(@"C:\work\webspider\Bo\App_Data\dict60k.txt");
			return _instance;
		}
		struct Entry
		{
			public string Key;
			public int Length;
			public int[] Stats;
			public int Hash;
		}

		int GetHash(int[] stats)
		{
			return string.Join(",", stats).GetHashCode();
		}

		int[] GetStats(string input)
		{
			var stats = new int[26];
			foreach (var y in input.ToUpper())
			{
				stats[y - 'A']++;
			}
			return stats;
		}

		//string Expression = "^[a-zA-Z]+$";
		Regex regex = new Regex("^[a-zA-Z]+$");
		Regex regexClean = new Regex("[^a-zA-Z]");

		List<Entry> Data;
		private Dict(string fileName)
		{
			
			var lines = System.IO.File.ReadAllLines(fileName).Where(x => regex.IsMatch(x));
			Data = lines.Select(x =>
			{
				var stats = new int[26];
				foreach (var y in x.ToUpper())
				{
					stats[y - 'A']++;
				}
				return new Entry() { Key = x, Length = x.Length, Stats = stats, Hash = GetHash(stats) };
			}).ToList();
		}

		public List<string> Guess(string input, int? size)
		{
			input = regexClean.Replace(input, "");
			var stats = GetStats(input);
			int hash = GetHash(stats);
			IEnumerable<Entry> data = size.HasValue ? Data.Take(size.Value) : Data;
			if (data.Count(x => x.Hash == hash) > 0)
				return data.Where(x => x.Hash == hash).Select(x => x.Key).ToList();

			int len = stats.Aggregate((s,x)=> s+=x);
			var result = data.Where(x => x.Length <= len);
			for(int i=0;i<stats.Length;i++)
			{
				int seq = i;
				result = result.Where(x => x.Stats[seq] <= stats[seq]);
			}

			return result.Select(x => x.Key).ToList();
		}
		public List<string> Guess(string input, string existing, int? size)
		{
			if (string.IsNullOrEmpty(existing))
				return Guess(input, size);

			input = regexClean.Replace(input, "");
			existing = regexClean.Replace(existing, "").ToUpper();
			var chars = existing.Distinct().ToList();

			IEnumerable<Entry> data = size.HasValue ?  Data.Take(size.Value):Data;

			var stats = GetStats(input);
			var list = new List<string>();
			foreach(var c in chars)
			{
				stats[c - 'A']++;
				int hash = GetHash(stats);
				if (data.Count(x => x.Hash == hash) > 0)
					list.Add(data.First(x => x.Hash == hash).Key);
				stats[c - 'A']--;
			}
			if (list.Count > 0)
				return list;

			int len = stats.Aggregate((s, x) => s += x);
			var result = data.Where(x => x.Length <= len + 1);

			List<IEnumerable<Entry>> final = new List<IEnumerable<Entry>>();

			for(int j=0;j<chars.Count;j++)
			{
				var batch = result;
				for (int i = 0; i < stats.Length; i++)
				{
					int seq = i;
					bool add = (chars[j] - 'A' == seq);
					if(add)
						batch = batch.Where(x => (x.Stats[seq] <= stats[seq] + 1) && (x.Stats[seq]>0));
					else
						batch = batch.Where(x => (x.Stats[seq] <= stats[seq] ));
				}
				final.Add(batch);
			}

			return final.SelectMany(x => x.Select(y=>y.Key)).ToList();
		}

	}
}
