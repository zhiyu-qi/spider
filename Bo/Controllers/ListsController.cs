﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Bo.Data;

namespace Bo.Controllers
{
	public class ListsController : ApiController
	{
		private static object locker = new object();

		private tmEntities db = new tmEntities();
		private static List<Tuple<int, string>> allMaker = null;
		private static List<Tuple<int, int, string>> allModel = null;

		private static void Init()
		{
			lock (locker)
			{
				if (allMaker != null)
					return;
				tmEntities db0 = new tmEntities();
				allMaker = db0.Makers.Select(x => new { x.MakerId, x.MakerName }).ToList().
					Select(x => new Tuple<int, string>(x.MakerId, x.MakerName)).ToList();
				allModel = db0.Models.Select(x => new { x.ModelId, x.MakerId.Value, x.ModelName }).ToList().
					Select(x => new Tuple<int, int, string>(x.ModelId, x.Value, x.ModelName)).ToList();
			}
		}

		// GET: api/Lists
		public IQueryable<List> GetLists(int? page, int? size)
		{
			if (page == null || page < 1)
				page = 1;
			if (size == null || size < 1)
				size = 50;

			return db.Lists.OrderBy(x => x.ListId).Skip(size.Value * (page.Value - 1)).Take(size.Value);
		}

		// GET: api/Lists/5
		[ResponseType(typeof(List))]
		public IHttpActionResult GetList(int id)
		{
			List list = db.Lists.Find(id);
			if (list == null)
			{
				return NotFound();
			}

			return Ok(list);
		}

		// PUT: api/Lists/5
		[ResponseType(typeof(void))]
		public IHttpActionResult PutList(int id, List list)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != list.ListId)
			{
				return BadRequest();
			}

			db.Entry(list).State = EntityState.Modified;

			try
			{
				db.SaveChanges();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!ListExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return StatusCode(HttpStatusCode.NoContent);
		}

		// POST: api/Lists
		[ResponseType(typeof(List))]
		public IHttpActionResult PostList(List list)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (list.ModelId == 0)
			{
				Init();

				//parse title
				var maker = allMaker.FirstOrDefault(x => list.Title.StartsWith(x.Item2));
				if (maker == null)
					throw new Exception("Unknown maker!");

				var text = list.Title.Remove(0, maker.Item2.Length + 1);
				var model = allModel.FirstOrDefault(x => x.Item2 == maker.Item1 && text.StartsWith(x.Item3));
				if (model == null)
					throw new Exception("Unknown model!");
				list.ModelId = model.Item1;

				text = text.Remove(0, model.Item3.Length + 1);
				var year = text;
				if (text.Contains(" "))
				{
					year = text.Split(' ').Last();
					text = text.Remove(text.Length - year.Length - 1, year.Length + 1);
				}
				else
					text = "";

				list.ModelDetail = text;
				list.Yr = int.Parse(year);
			}

			if (list.CreatedAt.Year < 2000)
				list.CreatedAt = DateTime.Now;

			db.Lists.Add(list);

			try
			{
				db.SaveChanges();
			}
			catch (DbUpdateException)
			{
				if (ListExists(list.ListId))
				{
					return Conflict();
				}
				else
				{
					throw;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			return CreatedAtRoute("DefaultApi", new { id = list.ListId }, list);
		}

		// DELETE: api/Lists/5
		[ResponseType(typeof(List))]
		public IHttpActionResult DeleteList(int id)
		{
			List list = db.Lists.Find(id);
			if (list == null)
			{
				return NotFound();
			}

			db.Lists.Remove(list);
			db.SaveChanges();

			return Ok(list);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		private bool ListExists(int id)
		{
			return db.Lists.Count(e => e.ListId == id) > 0;
		}
	}
}