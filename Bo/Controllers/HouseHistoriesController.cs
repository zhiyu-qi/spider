﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Bo.Data;

namespace Bo.Controllers
{
    public class HouseHistoriesController : ApiController
    {
        private tmEntities db = new tmEntities();

        // GET: api/HouseHistories
        public IQueryable<HouseHistory> GetHouseHistories()
        {
            return db.HouseHistories;
        }

        // GET: api/HouseHistories/5
        [ResponseType(typeof(HouseHistory))]
        public IHttpActionResult GetHouseHistory(int id)
        {
            HouseHistory houseHistory = db.HouseHistories.Find(id);
            if (houseHistory == null)
            {
                return NotFound();
            }

            return Ok(houseHistory);
        }

        // PUT: api/HouseHistories/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHouseHistory(int id, HouseHistory houseHistory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != houseHistory.HouseHistoryId)
            {
                return BadRequest();
            }

            db.Entry(houseHistory).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HouseHistoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/HouseHistories
        [ResponseType(typeof(HouseHistory))]
        public IHttpActionResult PostHouseHistory(HouseHistory houseHistory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.HouseHistories.Add(houseHistory);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = houseHistory.HouseHistoryId }, houseHistory);
        }

        // DELETE: api/HouseHistories/5
        [ResponseType(typeof(HouseHistory))]
        public IHttpActionResult DeleteHouseHistory(int id)
        {
            HouseHistory houseHistory = db.HouseHistories.Find(id);
            if (houseHistory == null)
            {
                return NotFound();
            }

            db.HouseHistories.Remove(houseHistory);
            db.SaveChanges();

            return Ok(houseHistory);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HouseHistoryExists(int id)
        {
            return db.HouseHistories.Count(e => e.HouseHistoryId == id) > 0;
        }
    }
}