﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Bo.Data;

namespace Bo.Controllers
{
	public class HousesController : ApiController
    {
        private tmEntities db = new tmEntities();

        // GET: api/Houses
        public IQueryable<House> GetHouses()
        {
			//RebuildHistory();
            return db.Houses.Take(3);
        }

		private void RebuildHistory()
		{
			foreach (var house in db.Houses)
			{
				DateTime lastChange = house.Created;
				if (!string.IsNullOrEmpty(house.History))
				{
					var list = house.History.Split(new char[]{'\n'},StringSplitOptions.RemoveEmptyEntries);
					foreach (string his in list)
					{
						int pos = his.IndexOf('\t');

						var detail = new HouseHistory();
						detail.House = house;
						detail.ChangedAt = DateTime.Parse(his.Substring(0, pos));
						detail.Price = his.Substring(pos + 1);
						house.HouseHistories.Add(detail);
					}
					house.History = "";
				}
				else
				{
					var detail = new HouseHistory();
					detail.House = house;
					detail.ChangedAt = house.Created;
					detail.Price = house.Price;
					house.HouseHistories.Add(detail);
				}
			}
			db.SaveChanges();
		}

        // GET: api/Houses/5
		[Helper.AllowCrossSiteJson2(Origin="*")]
		[ResponseType(typeof(House))]
        public IHttpActionResult GetHouse(int id)
        {
            House house = db.Houses.Find(id);
            if (house == null)
            {
                return NotFound();
            }

            return Ok(house);
        }

        // PUT: api/Houses/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHouse(int id, House house)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != house.HouseId)
            {
                return BadRequest();
            }

            db.Entry(house).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HouseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Houses
        [ResponseType(typeof(House))]
        public IHttpActionResult PostHouse(House house)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Houses.Add(house);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (HouseExists(house.HouseId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = house.HouseId }, house);
        }

        // DELETE: api/Houses/5
        [ResponseType(typeof(House))]
        public IHttpActionResult DeleteHouse(int id)
        {
            House house = db.Houses.Find(id);
            if (house == null)
            {
                return NotFound();
            }

            db.Houses.Remove(house);
            db.SaveChanges();

            return Ok(house);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HouseExists(int id)
        {
            return db.Houses.Count(e => e.HouseId == id) > 0;
        }
    }
}