﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Bo.Data;

namespace Bo.Controllers
{
    public class MakersController : ApiController
    {
        private tmEntities db = new tmEntities();

        // GET: api/Makers
        public IQueryable<Maker> GetMakers()
        {
            return db.Makers;
        }

        // GET: api/Makers/5
        [ResponseType(typeof(Maker))]
        public IHttpActionResult GetMaker(int id)
        {
            Maker maker = db.Makers.Find(id);
            if (maker == null)
            {
                return NotFound();
            }

            return Ok(maker);
        }

        // PUT: api/Makers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMaker(int id, Maker maker)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != maker.MakerId)
            {
                return BadRequest();
            }

            db.Entry(maker).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MakerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Makers
        [ResponseType(typeof(Maker))]
        public IHttpActionResult PostMaker(Maker maker)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Makers.Add(maker);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (MakerExists(maker.MakerId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = maker.MakerId }, maker);
        }

        // DELETE: api/Makers/5
        [ResponseType(typeof(Maker))]
        public IHttpActionResult DeleteMaker(int id)
        {
            Maker maker = db.Makers.Find(id);
            if (maker == null)
            {
                return NotFound();
            }

            db.Makers.Remove(maker);
            db.SaveChanges();

            return Ok(maker);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MakerExists(int id)
        {
            return db.Makers.Count(e => e.MakerId == id) > 0;
        }
    }
}