﻿using System;
using System.Web.Http.Filters;
//using System.Web.Mvc;

namespace Bo.Helper
{
	public class AllowCrossSiteJson2Attribute : ActionFilterAttribute
	{
		public string Origin { get; set; }
		public override void OnActionExecuted(HttpActionExecutedContext context )
		{
			if (!string.IsNullOrEmpty(Origin) && context.Response != null)
			{
				context.Response.Headers.Add("Access-Control-Allow-Origin", Origin);
				context.Response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			}
			base.OnActionExecuted(context);
		}
	}
}