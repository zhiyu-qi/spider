﻿using System;
//using System.Web.Http.Filters;
using System.Web.Mvc;

namespace Bo.Helper
{
	public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
	{
		public string Origin { get; set; }
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			//if (filterContext.ActionParameters.ContainsKey("Origin"))
			//{
			//	Origin = filterContext.ActionParameters["Origin"] as string;
			//}
			//else
			//	Origin = "*";
		}

		public override void OnActionExecuted(ActionExecutedContext actionExecutedContext)
		{
			if (!string.IsNullOrEmpty(Origin) && actionExecutedContext.HttpContext.Response != null)
			{
				actionExecutedContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", Origin);
				actionExecutedContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			}
			base.OnActionExecuted(actionExecutedContext);
		}
	}
}