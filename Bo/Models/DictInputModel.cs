﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Bo.Models
{
	public class DictInputModel
	{
		[Required]
		[StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
		public string Letters { get; set; }
		public string Leading { get; set; }
	}
}